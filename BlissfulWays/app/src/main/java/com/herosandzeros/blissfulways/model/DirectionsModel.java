package com.herosandzeros.blissfulways.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mathan on 26/7/15.
 */
public class DirectionsModel {

    private LatLng mStartLatLng;
    private LatLng mEndLatLng;
    private LatLng mAttractionLatLng;
    private String mAttractionName;

    public String getAttractionName() {
        return mAttractionName;
    }

    public void setAttractionName(String attractionName) {
        mAttractionName = attractionName;
    }

    public LatLng getStartLatLng() {
        return mStartLatLng;
    }

    public void setStartLatLng(LatLng startLatLng) {
        mStartLatLng = startLatLng;
    }

    public LatLng getEndLatLng() {
        return mEndLatLng;
    }

    public void setEndLatLng(LatLng endLatLng) {
        mEndLatLng = endLatLng;
    }

    public LatLng getAttractionLatLng() {
        return mAttractionLatLng;
    }

    public void setAttractionLatLng(LatLng attractionLatLng) {
        mAttractionLatLng = attractionLatLng;
    }
}

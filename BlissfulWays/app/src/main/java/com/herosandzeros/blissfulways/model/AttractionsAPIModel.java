package com.herosandzeros.blissfulways.model;

import com.google.gson.annotations.Expose;

/**
 * Created by pravinraj on 26/07/15.
 */
public class AttractionsAPIModel {

    @Expose
    private String origin;
    @Expose
    private String destination;
    @Expose
    private String type;

    /**
     *
     * @return
     * The origin
     */
    public String getOrigin() {
        return origin;
    }

    /**
     *
     * @param origin
     * The origin
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     *
     * @return
     * The destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     *
     * @param destination
     * The destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }


}

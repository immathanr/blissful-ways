package com.herosandzeros.blissfulways.api;

import com.herosandzeros.blissfulways.model.LoginAPIModel;
import com.herosandzeros.blissfulways.model.LoginResultModel;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by pravinraj on 26/07/15.
 */

public interface LoginAPI {

    @POST("/auth/addDevice")
    public void login(@Body LoginAPIModel LoginAPIModel, Callback<LoginResultModel> response);
}
package com.herosandzeros.blissfulways.activities;

import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.herosandzeros.blissfulways.R;
import com.herosandzeros.blissfulways.adapter.PlaceAutocompleteAdapter;
import com.herosandzeros.blissfulways.model.AttractionsAPIModel;
import com.herosandzeros.blissfulways.model.SearchParams;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener,
        OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    protected Location mCurrentLocation;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.fromTextView)
    AutoCompleteTextView mFromLocation;
    @InjectView(R.id.toTextView)
    AutoCompleteTextView mToLocation;
    @InjectView(R.id.happyButton)
    ImageButton happyButton;
    @InjectView(R.id.musicButton)
    ImageButton musicButton;
    @InjectView(R.id.hungryButton)
    ImageButton hungryButton;
    @InjectView(R.id.drinksButton)
    ImageButton drinksButton;
    @InjectView(R.id.search)
    FloatingActionButton mSearch;
    private PlaceAutocompleteAdapter mAdapter;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private EventBus mEventBus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.inject(this);
        buildGoogleApiClient();
        // Set a toolbar to replace the action bar.
        toolbar.setTitle(getString(R.string.search_str));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        happyButton.setOnClickListener(this);
        hungryButton.setOnClickListener(this);
        musicButton.setOnClickListener(this);
        drinksButton.setOnClickListener(this);

        LatLngBounds mBounds = new LatLngBounds(new LatLng(8.565242, 66.717212),
                new LatLng(36.160479, 95.940844));
        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.city_list_item, R.id.city_name,
                mGoogleApiClient, mBounds, null);
        mFromLocation.setAdapter(mAdapter);
        mToLocation.setAdapter(mAdapter);
        mFromLocation.setText(R.string.your_location);
        mToLocation.requestFocus();
    }

    public void resetAllImages() {
        happyButton.setSelected(false);
        happyButton.setImageResource(R.mipmap.happy_grey);
        hungryButton.setSelected(false);
        hungryButton.setImageResource(R.mipmap.hungry_grey);
        musicButton.setSelected(false);
        musicButton.setImageResource(R.mipmap.music_grey);
        drinksButton.setSelected(false);
        drinksButton.setImageResource(R.mipmap.drinks_grey);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        createLocationRequest();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (mCurrentLocation != null) {

            }
        }
        startLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        mCurrentLocation = location;
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (mCurrentLocation != null) {

            }
        }
        stopLocationUpdates();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SearchActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @OnClick(R.id.search)
    public void search() {
        if (mFromLocation.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), R.string.enter_from_location, Toast
                    .LENGTH_SHORT).show();
            return;
        }
        if (mToLocation.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), R.string.enter_to_location, Toast
                    .LENGTH_SHORT).show();
            return;
        }
        if (!isMoodSelected()) {
            Toast.makeText(getApplicationContext(), R.string.selecte_mood, Toast
                    .LENGTH_SHORT).show();
            return;
        }
        if (mFromLocation.getText().toString().equalsIgnoreCase(getString(R.string.your_location))) {
            getAddress();
        } else {
            sendValuesToSearch();
        }
    }

    private void sendValuesToSearch() {
        AttractionsAPIModel attractionsAPIModel = new AttractionsAPIModel();
        String[] fromAddressArray = mFromLocation.getText().toString().split(",");
        String[] toAddressArray = mToLocation.getText().toString().split(",");


        attractionsAPIModel.setOrigin(fromAddressArray[0]);
        attractionsAPIModel.setDestination(toAddressArray[0]);
        attractionsAPIModel.setType(getMood());
        SearchParams searchParams = new SearchParams();
        searchParams.setFromAddress(fromAddressArray[0]);
        searchParams.setToAddress(toAddressArray[0]);
        String mood = "";
        if (happyButton.isSelected()) {
            mood = "happy";
        }
        if (musicButton.isSelected()) {
            mood = "music";
        }
        if (hungryButton.isSelected()) {
            mood = "restaurant";
        }
        if (drinksButton.isSelected()) {
            mood = "drink";
        }
        searchParams.setMood(mood);
        mEventBus.postSticky(attractionsAPIModel);
        startActivity(new Intent(SearchActivity.this, ListActivity.class));
    }

    private String getMood() {
        String mood = "";
        if (happyButton.isSelected()) {
            mood = "happy";
        }
        else if (musicButton.isSelected()) {
            mood = "music";
        }
        else if (hungryButton.isSelected()) {
            mood = "restaurant";
        }
        else if (drinksButton.isSelected()) {
            mood = "drink";
        }
        return mood;

    }

    private void sendValues(String value) {
        String[] fromAddressArray = value.split(",");
        String[] toAddressArray = mToLocation.getText().toString().split(",");
        AttractionsAPIModel attractionsAPIModel = new AttractionsAPIModel();
        attractionsAPIModel.setOrigin(fromAddressArray[0]);
//        searchParams.setFromAddress(fromAddressArray[0]);
//        searchParams.setToAddress(toAddressArray[0]);
        attractionsAPIModel.setDestination(toAddressArray[0]);
        attractionsAPIModel.setType(getMood());
        SearchParams searchParams = new SearchParams();
        searchParams.setFromAddress(value);
        searchParams.setToAddress(mToLocation.getText().toString());
        String mood = "";
        if (happyButton.isSelected()) {
            mood = "happy";
        }
        if (musicButton.isSelected()) {
            mood = "music";
        }
        if (hungryButton.isSelected()) {
            mood = "restaurant";
        }
        if (drinksButton.isSelected()) {
            mood = "drink";
        }
        searchParams.setMood(mood);
        mEventBus.postSticky(attractionsAPIModel);

        startActivity(new Intent(SearchActivity.this, ListActivity.class));
    }

    private boolean isMoodSelected() {
        if (happyButton.isSelected() || musicButton.isSelected() || hungryButton.isSelected() ||
                drinksButton.isSelected()) {
            return true;
        } else {
            return false;
        }
    }

    private void getAddress() {
        Geocoder geocoder;
        StringBuilder address = new StringBuilder();
        List<Address> yourAddress = null;
        geocoder = new Geocoder(this, Locale.getDefault());
        if (mCurrentLocation != null) {
            try {
                yourAddress = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation
                        .getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (yourAddress != null && yourAddress.size() > 0) {
                address.append(yourAddress.get(0).getAddressLine(0));
                address.append(yourAddress.get(0).getAddressLine(1));
                address.append(yourAddress.get(0).getAddressLine(2));
            }
        }
        sendValues(address.toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.happyButton:
                resetAllImages();
                happyButton.setImageResource(R.mipmap.happy);
                happyButton.setSelected(true);
                break;
            case R.id.musicButton:
                resetAllImages();
                musicButton.setImageResource(R.mipmap.music);
                musicButton.setSelected(true);
                break;
            case R.id.hungryButton:
                resetAllImages();
                hungryButton.setImageResource(R.mipmap.hungry);
                hungryButton.setSelected(true);
                break;
            case R.id.drinksButton:
                resetAllImages();
                drinksButton.setImageResource(R.mipmap.drinks);
                drinksButton.setSelected(true);
                break;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}

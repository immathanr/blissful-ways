package com.herosandzeros.blissfulways.model;

import com.google.gson.annotations.Expose;

/**
 * Created by pravinraj on 26/07/15.
 */
public class LoginAPIModel {

    @Expose
    private String email;
    @Expose
    private String gcmId;
    @Expose
    private String deviceId;

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The gcmId
     */
    public String getGcmId() {
        return gcmId;
    }

    /**
     *
     * @param gcmId
     * The gcmId
     */
    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    /**
     *
     * @return
     * The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     *
     * @param deviceId
     * The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}

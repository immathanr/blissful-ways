package com.herosandzeros.blissfulways;

import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

/**
 * Created by pravinraj on 25/07/15.
 */

public class Initializor extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Permission[] permissions = new Permission[]{
                Permission.PUBLIC_PROFILE,
                Permission.EMAIL,
        };

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId("706303782832108")
                .setNamespace("blissfulways")
                .setPermissions(permissions)
                .build();


        SimpleFacebook.setConfiguration(configuration);
    }

}

package com.herosandzeros.blissfulways.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.herosandzeros.blissfulways.MapViewActivity;
import com.herosandzeros.blissfulways.R;
import com.herosandzeros.blissfulways.api.LoginAPI;
import com.herosandzeros.blissfulways.model.LoginAPIModel;
import com.herosandzeros.blissfulways.model.LoginResultModel;
import com.herosandzeros.blissfulways.utils.AppConstants;
import com.herosandzeros.blissfulways.utils.BlissfulPreference;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignUpActivity extends AppCompatActivity implements OnLoginListener, View.OnClickListener {

    SimpleFacebook mSimpleFacebook;
    @InjectView(R.id.loginButton)
    Button loginButton;
    RestAdapter restAdapter;
    LoginAPI loginapi;
    BlissfulPreference pref;
    private String TAG = SignUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.inject(this);

        pref = new BlissfulPreference(this);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(AppConstants.ENDPOINT).build();

        loginapi = restAdapter.create(LoginAPI.class);

        loginButton.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    @Override
    public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
        Log.i(TAG, "Logged in");
        OnProfileListener onProfileListener = new OnProfileListener() {
            @Override
            public void onComplete(final Profile profile) {


                Log.v(TAG, "Profile On Complete");

                LoginAPIModel loginapiModel = new LoginAPIModel();
                loginapiModel.setEmail(profile.getId());
                loginapiModel.setDeviceId(Settings.Secure.getString(SignUpActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID));
                loginapiModel.setGcmId("1234567890");


                loginapi.login(loginapiModel, new Callback<LoginResultModel>() {
                    @Override
                    public void success(LoginResultModel loginResultModel, Response response) {
                        Log.v(TAG, String.valueOf(loginResultModel.isResponse()));
                        if(loginResultModel.isResponse()){

                            pref.setLoginStatus(true);
                            pref.setName(profile.getName());
                            pref.setProfilePic(profile.getPicture());
                            pref.setAccessToken(loginResultModel.getData().getDeviceAccess().getApiToken());

                            startActivity(new Intent(SignUpActivity.this, MapViewActivity.class));
                            finish();

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Log.v(TAG,error.toString());

                    }
                });
            }
        };

        PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
        pictureAttributes.setHeight(500);
        pictureAttributes.setWidth(500);
        pictureAttributes.setType(PictureAttributes.PictureType.SQUARE);

        Profile.Properties properties = new Profile.Properties.Builder()
                .add(Profile.Properties.ID)
                .add(Profile.Properties.NAME)
                .add(Profile.Properties.EMAIL)
                .add(Profile.Properties.PICTURE, pictureAttributes)
                .build();

        mSimpleFacebook.getProfile(properties, onProfileListener);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onException(Throwable throwable) {

    }

    @Override
    public void onFail(String s) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginButton :
                mSimpleFacebook.login(this);
                break;
        }
    }
}

package com.herosandzeros.blissfulways.model;

import com.google.gson.annotations.Expose;

/**
 * Created by pravinraj on 26/07/15.
 */
public class LoginResultModel {

        @Expose
        private boolean response;
        @Expose
        private String msg;
        @Expose
        private Data data;

        /**
         *
         * @return
         * The response
         */
        public boolean isResponse() {
            return response;
        }

        /**
         *
         * @param response
         * The response
         */
        public void setResponse(boolean response) {
            this.response = response;
        }

        /**
         *
         * @return
         * The msg
         */
        public String getMsg() {
            return msg;
        }

        /**
         *
         * @param msg
         * The msg
         */
        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         *
         * @return
         * The data
         */
        public Data getData() {
            return data;
        }

        /**
         *
         * @param data
         * The data
         */
        public void setData(Data data) {
            this.data = data;
        }



    public class DeviceAccess {

        @Expose
        private String key;
        @Expose
        private String email;
        @Expose
        private String deviceId;
        @Expose
        private String gcmId;
        @Expose
        private long created;
        @Expose
        private String apiToken;
        @Expose
        private long updated;

        /**
         *
         * @return
         * The key
         */
        public String getKey() {
            return key;
        }

        /**
         *
         * @param key
         * The key
         */
        public void setKey(String key) {
            this.key = key;
        }

        /**
         *
         * @return
         * The email
         */
        public String getEmail() {
            return email;
        }

        /**
         *
         * @param email
         * The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         *
         * @return
         * The deviceId
         */
        public String getDeviceId() {
            return deviceId;
        }

        /**
         *
         * @param deviceId
         * The deviceId
         */
        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        /**
         *
         * @return
         * The gcmId
         */
        public String getGcmId() {
            return gcmId;
        }

        /**
         *
         * @param gcmId
         * The gcmId
         */
        public void setGcmId(String gcmId) {
            this.gcmId = gcmId;
        }

        /**
         *
         * @return
         * The created
         */
        public long getCreated() {
            return created;
        }

        /**
         *
         * @param created
         * The created
         */
        public void setCreated(long created) {
            this.created = created;
        }

        /**
         *
         * @return
         * The apiToken
         */
        public String getApiToken() {
            return apiToken;
        }

        /**
         *
         * @param apiToken
         * The apiToken
         */
        public void setApiToken(String apiToken) {
            this.apiToken = apiToken;
        }

        /**
         *
         * @return
         * The updated
         */
        public long getUpdated() {
            return updated;
        }

        /**
         *
         * @param updated
         * The updated
         */
        public void setUpdated(long updated) {
            this.updated = updated;
        }

    }

    public class Data {

        @Expose
        private DeviceAccess deviceAccess;

        /**
         *
         * @return
         * The deviceAccess
         */
        public DeviceAccess getDeviceAccess() {
            return deviceAccess;
        }

        /**
         *
         * @param deviceAccess
         * The deviceAccess
         */
        public void setDeviceAccess(DeviceAccess deviceAccess) {
            this.deviceAccess = deviceAccess;
        }

    }


}

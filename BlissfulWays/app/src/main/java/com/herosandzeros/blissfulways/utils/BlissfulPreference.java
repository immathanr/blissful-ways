package com.herosandzeros.blissfulways.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pravinraj on 25/07/15.
 */

public class BlissfulPreference {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;

    public BlissfulPreference(Context context) {

        this.context = context;

        preferences = context.getSharedPreferences(AppConstants.PREF_NAME, AppConstants.PRIVATE_MODE);
        editor = preferences.edit();

    }

    public void setLoginStatus (Boolean bool){

        editor.putBoolean(AppConstants.IS_LOGGED_IN, bool);
        editor.commit();

    }

    public Boolean isLoggedIn (){

        return preferences.getBoolean(AppConstants.IS_LOGGED_IN, false);

    }

    public void setName (String data){

        editor.putString(AppConstants.NAME, data);
        editor.commit();

    }

    public String getName (){

        return preferences.getString(AppConstants.NAME, null);

    }

    public void setAccessToken (String data){

        editor.putString(AppConstants.ACCESSTOKEN, data);
        editor.commit();

    }

    public String getAccessToken (){

        return preferences.getString(AppConstants.ACCESSTOKEN, null);

    }

    public void setProfilePic (String data){

        editor.putString(AppConstants.PROFILEPIC, data);
        editor.commit();

    }

    public String getProfilePic (){

        return preferences.getString(AppConstants.PROFILEPIC, null);

    }

    public void logout() {
        editor.clear();
        editor.commit();
    }
}

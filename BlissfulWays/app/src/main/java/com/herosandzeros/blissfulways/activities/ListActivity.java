package com.herosandzeros.blissfulways.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.herosandzeros.blissfulways.R;
import com.herosandzeros.blissfulways.api.AttractionsAPI;
import com.herosandzeros.blissfulways.api.LoginAPI;
import com.herosandzeros.blissfulways.model.AttractionsAPIModel;
import com.herosandzeros.blissfulways.model.AttractionsResultModel;
import com.herosandzeros.blissfulways.model.DirectionsModel;
import com.herosandzeros.blissfulways.utils.AppConstants;
import com.herosandzeros.blissfulways.utils.BlissfulPreference;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListActivity extends AppCompatActivity {

    RestAdapter restAdapter;
    BlissfulPreference pref;
    AttractionsAPI attractionsAPI;
    AttractionsAPIModel attractionsAPIModel;
    private EventBus mEventBus = EventBus.getDefault();

    public static final String TAG = ListActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private AttractionsResultModel.Data mAttData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        mEventBus.registerSticky(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        pref = new BlissfulPreference(this);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(AppConstants.ENDPOINT).build();

        attractionsAPI = restAdapter.create(AttractionsAPI.class);

        attractionsAPI.getAttractions(pref.getAccessToken(), attractionsAPIModel, new Callback<AttractionsResultModel>() {
            @Override
            public void success(AttractionsResultModel attractionsResultModel, Response response) {

                if (attractionsResultModel.isResponse()) {
                    Log.v(TAG, String.valueOf(attractionsResultModel.isResponse()));
                    mAdapter = new MyAdapter(attractionsResultModel.getData());
                    mRecyclerView.setAdapter(mAdapter);

                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.v(TAG, error.toString());
            }
        });
    }

    public void onEvent(AttractionsAPIModel attractionsAPIModel) {
        this.attractionsAPIModel = attractionsAPIModel;
        Log.d("TAG",attractionsAPIModel.getDestination());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private AttractionsResultModel.Data mDataset;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView mTextView;
            public ViewHolder(LinearLayout v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.info_text);
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(AttractionsResultModel.Data myDataset) {
            mDataset = myDataset;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_row, parent, false);
            v.setOnClickListener(new MyOnClickListener());
            // set the view's size, margins, paddings and layout parameters
            ViewHolder vh = new ViewHolder((LinearLayout) v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            holder.mTextView.setText(mDataset.getAttractions().get(position).getName());

        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            Log.v(TAG, String.valueOf(mDataset.getAttractions().size()));
            return mDataset.getAttractions().size();
        }
    }

    class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int itemPosition = mRecyclerView.getChildPosition(v);
            Log.e("Clicked and ",String.valueOf(itemPosition));
            if(mAttData != null && mAttData.getPoints() != null) {
                DirectionsModel directionsModel = new DirectionsModel();
                directionsModel.setStartLatLng(new LatLng(Double.parseDouble(mAttData.getPoints().get(0).getStartLat()),Double.parseDouble(mAttData.getPoints().get(0).getStartLng())));
                directionsModel.setAttractionLatLng(new LatLng(Double.parseDouble(mAttData.getAttractions().get(itemPosition).getLat()),Double.parseDouble(mAttData.getAttractions().get(itemPosition).getLng())));
                directionsModel.setEndLatLng(new LatLng(Double.parseDouble(mAttData.getPoints().get(mAttData.getPoints().size() - 1).getEndLat()), Double.parseDouble(mAttData.getPoints().get(mAttData.getPoints().size() - 1).getEndLng())));
                mEventBus.postSticky(directionsModel);
            }

        }
    }


}

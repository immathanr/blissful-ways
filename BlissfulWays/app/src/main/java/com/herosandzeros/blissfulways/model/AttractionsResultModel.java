package com.herosandzeros.blissfulways.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pravinraj on 26/07/15.
 */
public class AttractionsResultModel {

        @Expose
        private boolean response;
        @Expose
        private Data data;

        /**
         *
         * @return
         * The response
         */
        public boolean isResponse() {
            return response;
        }

        /**
         *
         * @param response
         * The response
         */
        public void setResponse(boolean response) {
            this.response = response;
        }

        /**
         *
         * @return
         * The data
         */
        public Data getData() {
            return data;
        }

        /**
         *
         * @param data
         * The data
         */
        public void setData(Data data) {
            this.data = data;
        }

    public class Data {

        @Expose
        private List<Attraction> attractions = new ArrayList<Attraction>();
        @Expose
        private List<Point> points = new ArrayList<Point>();

        /**
         *
         * @return
         * The attractions
         */
        public List<Attraction> getAttractions() {
            return attractions;
        }

        /**
         *
         * @param attractions
         * The attractions
         */
        public void setAttractions(List<Attraction> attractions) {
            this.attractions = attractions;
        }

        /**
         *
         * @return
         * The points
         */
        public List<Point> getPoints() {
            return points;
        }

        /**
         *
         * @param points
         * The points
         */
        public void setPoints(List<Point> points) {
            this.points = points;
        }

    }

    public class Attraction {

        @Expose
        private String placeId;
        @Expose
        private String lat;
        @Expose
        private String lng;
        @Expose
        private String name;

        /**
         *
         * @return
         * The placeId
         */
        public String getPlaceId() {
            return placeId;
        }

        /**
         *
         * @param placeId
         * The placeId
         */
        public void setPlaceId(String placeId) {
            this.placeId = placeId;
        }

        /**
         *
         * @return
         * The lat
         */
        public String getLat() {
            return lat;
        }

        /**
         *
         * @param lat
         * The lat
         */
        public void setLat(String lat) {
            this.lat = lat;
        }

        /**
         *
         * @return
         * The lng
         */
        public String getLng() {
            return lng;
        }

        /**
         *
         * @param lng
         * The lng
         */
        public void setLng(String lng) {
            this.lng = lng;
        }

        /**
         *
         * @return
         * The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         * The name
         */
        public void setName(String name) {
            this.name = name;
        }

    }

    public class Point {

        @Expose
        private String startLat;
        @Expose
        private String startLng;
        @Expose
        private String endLat;
        @Expose
        private String endLng;
        @Expose
        private List<Object> places = new ArrayList<Object>();

        /**
         *
         * @return
         * The startLat
         */
        public String getStartLat() {
            return startLat;
        }

        /**
         *
         * @param startLat
         * The startLat
         */
        public void setStartLat(String startLat) {
            this.startLat = startLat;
        }

        /**
         *
         * @return
         * The startLng
         */
        public String getStartLng() {
            return startLng;
        }

        /**
         *
         * @param startLng
         * The startLng
         */
        public void setStartLng(String startLng) {
            this.startLng = startLng;
        }

        /**
         *
         * @return
         * The endLat
         */
        public String getEndLat() {
            return endLat;
        }

        /**
         *
         * @param endLat
         * The endLat
         */
        public void setEndLat(String endLat) {
            this.endLat = endLat;
        }

        /**
         *
         * @return
         * The endLng
         */
        public String getEndLng() {
            return endLng;
        }

        /**
         *
         * @param endLng
         * The endLng
         */
        public void setEndLng(String endLng) {
            this.endLng = endLng;
        }

        /**
         *
         * @return
         * The places
         */
        public List<Object> getPlaces() {
            return places;
        }

        /**
         *
         * @param places
         * The places
         */
        public void setPlaces(List<Object> places) {
            this.places = places;
        }

    }

}

package com.herosandzeros.blissfulways.api;

import com.herosandzeros.blissfulways.model.AttractionsAPIModel;
import com.herosandzeros.blissfulways.model.AttractionsResultModel;
import com.herosandzeros.blissfulways.model.LoginAPIModel;
import com.herosandzeros.blissfulways.model.LoginResultModel;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.POST;

/**
 * Created by pravinraj on 26/07/15.
 */
public interface AttractionsAPI {

    @POST("/product/getStepsBetween")
    public void getAttractions(@Header("Blissful-ApiKey") String authorization, @Body AttractionsAPIModel attractionsAPIModel, Callback<AttractionsResultModel> response);

}

package com.herosandzeros.blissfulways.model;

/**
 * Created by mathan on 26/7/15.
 */
public class SearchParams {

    private String mFromAddress;
    private String mToAddress;
    private String mMood;

    public String getFromAddress() {
        return mFromAddress;
    }

    public void setFromAddress(String fromAddress) {
        mFromAddress = fromAddress;
    }

    public String getToAddress() {
        return mToAddress;
    }

    public void setToAddress(String toAddress) {
        mToAddress = toAddress;
    }

    public String getMood() {
        return mMood;
    }

    public void setMood(String mood) {
        mMood = mood;
    }
}

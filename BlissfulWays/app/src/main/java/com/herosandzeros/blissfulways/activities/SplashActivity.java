package com.herosandzeros.blissfulways.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.herosandzeros.blissfulways.MapViewActivity;
import com.herosandzeros.blissfulways.R;
import com.herosandzeros.blissfulways.utils.BlissfulPreference;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

public class SplashActivity extends AppCompatActivity {

    BlissfulPreference pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        pref = new BlissfulPreference(this);

        if (!pref.isLoggedIn()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(SplashActivity.this, SignUpActivity.class));
                    finish();

                }
            }, 1500);

        }

        else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(SplashActivity.this, MapViewActivity.class));
                    finish();

                }
            }, 1500);

        }

    }
}

package com.herosandzeros.blissfulways.utils;

/**
 * Created by pravinraj on 26/07/15.
 */
public class AppConstants {
    public static final String PREF_NAME = "blissful_ways";
    public static final int PRIVATE_MODE = 0;

    public static final String ENDPOINT = "http://mygolaapp.appspot.com//api/v1";

    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String NAME = "name";
    public static final String ACCESSTOKEN = "access_token";
    public static final String PROFILEPIC = "profile_pic";
}
